package dmcs;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

class AdvancedStringCalculatorTest {

    private AdvancedStringCalculator calculator = new AdvancedStringCalculator();

    @Test
    void test1() {
        compute("3.50", "1 + 2.5");
        compute("25.00", "5 ^ 2");
        compute("9.00", "3 * 3");
        compute("2.95", "5 - 2.05");
        compute("3.00", "9 / 3");
    }

    @Test
    void test2() {
        compute("6.60","2.2 *  3");
    }

    @Test
    void test3() {
        compute("19.50","1.5 + 2 * 3 ^ 2");
    }

    @Test
    void test4() {
        compute("31.50","( 1.5 + 2 ) * 3 ^ 2");
        compute("1296.00","( ( 2.4 – 0.4 ) * 3 ) ^ 4");
    }

    @Test
    void test5() {
        IncorrectInputData incorrectInputData = assertThrows(IncorrectInputData.class, () -> calculator.compute("1 -* 3"));
        assertEquals("Missing number between - and * operators", incorrectInputData.getMessage());

        IncorrectInputData invalidNumberDetected = assertThrows(IncorrectInputData.class, () -> calculator.compute("2a * 3"));
        assertEquals("Invalid number detected: a", invalidNumberDetected.getMessage());

        IncorrectInputData extraRigth = assertThrows(IncorrectInputData.class, () -> calculator.compute("12 * ( 3.4 + 6 ))"));
        assertEquals("An extra right parenthesis detected", extraRigth.getMessage());
    }

    @Test
    void test6() {
        DivideByZero divideByZero = assertThrows(DivideByZero.class, () -> calculator.compute("1 / ( 2 - 2 )"));
        assertEquals("Division by zero", divideByZero.getMessage());
    }

    @Test
    void test7() {
        compute("1.50", "2 * 75%");
    }

    private void compute(String excepted, String input) {
        assertEquals(excepted, calculator.compute(input));
    }
}
