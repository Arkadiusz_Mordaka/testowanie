package dmcs;

import java.util.*;

class AdvancedStringCalculator {

    private static double r;
    private Stack<String> stack;
    private Queue<String> queue;


    String compute(String iOText) {
        getPostFix(iOText);
        return evaluatePostFix();
    }

    //getPostFix converts infix to postfix i.e. 3+2 == 32+
    private void getPostFix(String iOText) {
        Validator validator = new Validator();
        validator.validate(iOText);
        stack = new Stack<>();
        queue = new LinkedList<>();
        StringTokenizer t = new StringTokenizer(iOText);
        while (t.hasMoreTokens()) {
            String temp = t.nextToken();
            if (temp.equals("("))
                stack.push(temp);
            if (temp.equals(")")) {
                while (!stack.peek().equals("(")) {
                    queue.add(stack.pop());
                }
                if (stack.peek().equals("("))
                    stack.pop();
            }
            if (temp.equals("+") || temp.equals("-") || temp.equals("*") || temp.equals("/") || temp.equals("^")) {
                while (!stack.isEmpty() && !stack.peek().equals("(") && greaterOrEquals(stack.peek(), temp)) {
                    queue.add(stack.pop());
                }
                stack.push(temp);
            }
            if (isDouble(temp)) {
                queue.add(temp);
            }
        }
        while (!stack.isEmpty())
            queue.add(stack.pop());
    }

    private String evaluatePostFix() {
        while (!queue.isEmpty()) {
            String s = queue.peek();
            if (isDouble(s))
                stack.push(queue.remove());
            else if (queue.peek().equals(")")) {
                throw new IncorrectInputData("An extra left parenthesis detected");
            } else if (queue.peek().equals("(")) {
                throw new IncorrectInputData("An extra right parenthesis detected");
            } else if (queue.peek().equals("+") || queue.peek().equals("-") || queue.peek().equals("*")
                    || queue.peek().equals("/") || queue.peek().equals("^")) {
                calculate();
            }
        }
        String s = stack.pop();
        //stack should only have one element which is the answer.
        //now if it is not empty, then return ERROR
        //i.e. 3(2) == 32 , stack.pop will only pop off 2, leaving 3.
        if (!stack.isEmpty()) {
            throw new RuntimeException(stack.peek());
        }
        double value = Double.parseDouble(s);
        if(Double.isInfinite(value)){
            throw new DivideByZero("Division by zero");
        }
        return String.format("%.2f", value);
    }

    private void calculate() {
        double first = Double.parseDouble(stack.pop());
        double second = Double.parseDouble(stack.pop());
        String operator = queue.remove();
        switch (operator) {
            case "+":
                r = second + first;
                break;
            case "-":
                r = second - first;
                break;
            case "*":
                r = second * first;
                break;
            case "/":
                r = second / first;
                break;
            case "^":
                r = Math.pow(second, first);
                break;
        }
        stack.push(Double.toString(r));
    }

    private static boolean isDouble(String input) {
        try {
            Double.parseDouble(input);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    private boolean greaterOrEquals(Object a, String b) {
        if (a.equals("^"))
            return true;
        else if (a.equals("*") || a.equals("/")) {
            if (b.equals("^"))
                return false;
            return a.equals(b) || b.equals("+") || b.equals("-");
        } else if (a.equals("+") || a.equals("-")) {
            return a.equals(b);
        }
        return false;
    }
}