package dmcs;

class DivideByZero extends IllegalArgumentException {
    DivideByZero(String message) {
        super(message);
    }
}
