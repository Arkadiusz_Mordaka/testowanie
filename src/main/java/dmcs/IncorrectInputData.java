package dmcs;

public class IncorrectInputData extends IllegalArgumentException {
    public IncorrectInputData(String s) {
        super(s);
    }
}
