package dmcs;

import javax.swing.*;

public class Calculator extends JFrame {

    private Calculator() {
        AdvancedStringCalculator calculator = new AdvancedStringCalculator();
        String test = "( ( 2.4 – 0.4 ) * 3 ) ^ 4";
        String regularexpression = "1 / ( 2 - 2 )";
        String ouptu = calculator.compute(regularexpression);
        System.out.println(ouptu);
    }
    public static void main(String [] args) {
        new Calculator();
    }
}