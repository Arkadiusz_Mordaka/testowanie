package dmcs;

import java.util.Arrays;
import java.util.List;

class Validator {

    private final String alphabet = "abcdefghijklmnopqrstuvwxyz";

    void validate(String input){
        List<String> myData = Arrays.asList(input.split(""));
        for (int i=1 ; i < myData.size() ; i++) {
            if (myData.get(i-1).equals("-")  && myData.get(i).equals("-")) {
                throw new IncorrectInputData("Missing number between - and - operators");
            }
            if (myData.get(i-1).equals("-")  && myData.get(i).equals("+")) {
                throw new IncorrectInputData("Missing number between - and + operators");
            }
            if (myData.get(i-1).equals("-")  && myData.get(i).equals("*")) {
                throw new IncorrectInputData("Missing number between - and * operators");
            }
            if (myData.get(i-1).equals("-")  && myData.get(i).equals("/")) {
                throw new IncorrectInputData("Missing number between - and / operators");
            }
            if (myData.get(i-1).equals("-")  && myData.get(i).equals("^")) {
                throw new IncorrectInputData("Missing number between - and ^ operators");
            }
            if (myData.get(i-1).equals("+")  && myData.get(i).equals("-")) {
                throw new IncorrectInputData("Missing number between + and - operators");
            }
            if (myData.get(i-1).equals("+")  && myData.get(i).equals("+")) {
                throw new IncorrectInputData("Missing number between + and + operators");
            }
            if (myData.get(i-1).equals("+")  && myData.get(i).equals("*")) {
                throw new IncorrectInputData("Missing number between + and * operators");
            }
            if (myData.get(i-1).equals("+")  && myData.get(i).equals("/")) {
                throw new IncorrectInputData("Missing number between + and / operators");
            }
            if (myData.get(i-1).equals("+")  && myData.get(i).equals("^")) {
                throw new IncorrectInputData("Missing number between + and ^ operators");
            }
            if (myData.get(i-1).equals("*")  && myData.get(i).equals("-")) {
                throw new IncorrectInputData("Missing number between * and - operators");
            }
            if (myData.get(i-1).equals("*")  && myData.get(i).equals("+")) {
                throw new IncorrectInputData("Missing number between * and + operators");
            }
            if (myData.get(i-1).equals("*")  && myData.get(i).equals("*")) {
                throw new IncorrectInputData("Missing number between * and * operators");
            }
            if (myData.get(i-1).equals("*")  && myData.get(i).equals("/")) {
                throw new IncorrectInputData("Missing number between * and / operators");
            }
            if (myData.get(i-1).equals("*")  && myData.get(i).equals("^")) {
                throw new IncorrectInputData("Missing number between * and ^ operators");
            }
            if (myData.get(i-1).equals("/")  && myData.get(i).equals("-")) {
                throw new IncorrectInputData("Missing number between / and - operators");
            }
            if (myData.get(i-1).equals("/")  && myData.get(i).equals("+")) {
                throw new IncorrectInputData("Missing number between / and + operators");
            }
            if (myData.get(i-1).equals("/")  && myData.get(i).equals("*")) {
                throw new IncorrectInputData("Missing number between / and * operators");
            }
            if (myData.get(i-1).equals("/")  && myData.get(i).equals("/")) {
                throw new IncorrectInputData("Missing number between / and / operators");
            }
            if (myData.get(i-1).equals("/")  && myData.get(i).equals("^")) {
                throw new IncorrectInputData("Missing number between / and ^ operators");
            }
            if (myData.get(i-1).equals("^")  && myData.get(i).equals("-")) {
                throw new IncorrectInputData("Missing number between ^ and - operators");
            }
            if (myData.get(i-1).equals("^")  && myData.get(i).equals("+")) {
                throw new IncorrectInputData("Missing number between ^ and + operators");
            }
            if (myData.get(i-1).equals("^")  && myData.get(i).equals("*")) {
                throw new IncorrectInputData("Missing number between ^ and * operators");
            }
            if (myData.get(i-1).equals("^")  && myData.get(i).equals("/")) {
                throw new IncorrectInputData("Missing number between ^ and / operators");
            }
            if (myData.get(i-1).equals("^")  && myData.get(i).equals("^")) {
                throw new IncorrectInputData("Missing number between ^ and ^ operators");
            }
            for (char c = 'a'; c <= 'z'; c++) {
                if(myData.get(i-1).equals(Character.toString(c))){
                    throw new IncorrectInputData("Invalid number detected: " + c);
                }
            }
        }
    }

}
